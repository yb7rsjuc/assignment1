__author__ = 'Dalu_Yang'
import numpy as np
from sklearn import datasets, linear_model
import matplotlib.pyplot as plt

def generate_data():
    '''
    generate data
    :return: X: input data, y: given labels
    '''
    np.random.seed(0)
    X, y = datasets.make_moons(200, noise=0.20)
    return X, y

def plot_decision_boundary(pred_func, X, y):
    '''
    plot the decision boundary
    :param pred_func: function used to predict the label
    :param X: input data
    :param y: given labels
    :return:
    '''
    # Set min and max values and give it some padding
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
    plt.show()

########################################################################################################################
########################################################################################################################
# YOUR ASSSIGMENT STARTS HERE
# FOLLOW THE INSTRUCTION BELOW TO BUILD AND TRAIN A 3-LAYER NEURAL NETWORK
########################################################################################################################
########################################################################################################################
class DeepNeuralNetwork(object):
    """
    This class builds and trains a neural network
    """
    def __init__(self, network_topology, X, y, actFun_type='ReLU', reg_lambda=0.01, seed=0):
        '''
        :param nn_input_dim: input dimension
        :param nn_hidden_dim: the number of hidden units
        :param nn_output_dim: output dimension
        :param actFun_type: type of activation function. 3 options: 'tanh', 'sigmoid', 'relu'
        :param reg_lambda: regularization coefficient
        :param seed: random seed
        '''
        self.network_topology = network_topology
        self.actFun_type = actFun_type
        self.reg_lambda = reg_lambda
        self.X = X
        self.y = y
        
        
    def network_setup(self):#, network_topology, actFun_type, X):
        self.network = [Data(self.X)]
        np.random.seed(0)
        for no_neurons in self.network_topology:
            new_layer = Layer(no_neurons, self.network[-1], 
                              lambda x: self.actFun(x, type=self.actFun_type), 
                              lambda x: self.diff_actFun(x, type=self.actFun_type),
                             self.reg_lambda)
            self.network.append(new_layer)
            
            

    def actFun(self, z, type):
        '''
        actFun computes the activation functions
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: activations
        '''
        return{            
            'Tanh': (np.exp(2.*z)-1)/(np.exp(2.*z)+1),
            'Sigmoid': 1./(1.+np.exp(-z)),
            'ReLU': np.maximum(z,0),                                  
            }[type]

    def diff_actFun(self, z, type):
        '''
        diff_actFun computes the derivatives of the activation functions wrt the net input
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: the derivatives of the activation functions wrt the net input
        '''

        return{            
            'Tanh': 4.*np.exp(-2.*z)/(1.+np.exp(-2.*z))**2.,
            'Sigmoid': np.exp(z)/(np.exp(z)+1.)**2.,
            'ReLU': 0.5*(np.sign(z)+1.),                  
            }[type]


    def feedforward(self, X):
        '''
        feedforward builds a 3-layer neural network and computes the two probabilities,
        one for class 0 and one for class 1
        :param X: input data
        :param actFun: activation function
        :return:
        '''
        self.network[0].output = X
        for i in np.arange(1,len(self.network)):
            self.network[i].feedforward(self.network[i-1])
            
        exp_scores = np.exp(self.network[-1].z)
        self.probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        return None

    def calculate_loss(self, X, y):
        '''
        calculate_loss computes the loss for prediction
        :param X: input data
        :param y: given labels
        :return: the loss for prediction
        '''
        num_examples = len(X)
        num_classes = len(set(y))
        self.feedforward(X)
        # Calculating the loss
        y0 = np.zeros([num_examples, num_classes])
        y0[range(num_examples), y] = 1
        data_loss = -np.sum(np.multiply(y0, np.log(self.probs)))

        # Add regulatization term to loss (optional)
        # data_loss += self.reg_lambda / 2 * (np.sum(np.square(self.W1)) + np.sum(np.square(self.W2)))
        return (1. / num_examples) * data_loss

    def predict(self, X):
        '''
        predict infers the label of a given data point X
        :param X: input data
        :return: label inferred
        '''
        self.feedforward(X)
        return np.argmax(self.probs, axis=1)

    def backprop(self, X, y):
        '''
        backprop implements backpropagation to compute the gradients used to update the parameters in the backward step
        :param X: input data
        :param y: given labels
        :return: dL/dW1, dL/b1, dL/dW2, dL/db2
        '''

        # IMPLEMENT YOUR BACKPROP HERE
        num_examples = len(X)
        delta3 = self.probs
        delta3[range(num_examples), y] -= 1
        
        self.network[-1].delta = delta3
        self.network[-1].dW = np.dot(self.network[-1].x.T, delta3)
        self.network[-1].db = np.dot(np.ones([1, num_examples]), delta3)
        
        for i in np.arange(len(self.network)-2,0,-1):
            self.network[i].backprop(self.network[i+1], num_examples)
        
        return None
    
    def updateweight(self, learning_rate):
        
        for i in np.arange(1,len(self.network)):
            self.network[i].updateweight(learning_rate)
            
        return None
    
    def fit_model(self, X, y, learning_rate=0.0009, num_passes=10000, print_loss=True):
        '''
        fit_model uses backpropagation to train the network
        :param X: input data
        :param y: given labels
        :param num_passes: the number of times that the algorithm runs through the whole dataset
        :param print_loss: print the loss or not
        :return:
        '''
        # Gradient descent.
        for i in range(0, num_passes):
            # Forward propagation
            self.feedforward(X)
            # Backpropagation
            self.backprop(X, y)
            
            self.updateweight(learning_rate)
            
            
            # Add regularization terms (b1 and b2 don't have regularization terms)


            # Gradient descent parameter update

            
            # Optionally print the loss.
            # This is expensive because it uses the whole dataset, so we don't want to do it too often.
            if print_loss and i % 1000 == 0:
                print("Loss after iteration %i: %f" % (i, self.calculate_loss(X, y)))

    def visualize_decision_boundary(self, X, y):
        '''
        visualize_decision_boundary plots the decision boundary created by the trained network
        :param X: input data
        :param y: given labels
        :return:
        '''
        plot_decision_boundary(lambda x: self.predict(x), X, y)

        
class Layer(object):
    """
    This class defines a layer in network
    """
    def __init__(self, no_neurons, last_layer, actFun, diff_actFun, reg_lambda):
        self.output_dim = no_neurons
        self.input_dim = last_layer.output_dim
        self.W = np.random.randn(self.input_dim, self.output_dim) / np.sqrt(self.input_dim)
        self.b = np.zeros([1, self.output_dim])
        self.x = last_layer.output
        self.output = []
        self.actFun = actFun
        self.diff_actFun = diff_actFun
        self.reg_lambda = reg_lambda
        
        
    def feedforward(self, last_layer):
        self.x = last_layer.output
        self.z = np.dot(self.x, self.W) + self.b
        self.output = self.actFun(self.z)
        return None
    
    def backprop(self, next_layer, num_examples):
        self.delta = np.multiply(np.dot(next_layer.delta, next_layer.W.T),self.diff_actFun(self.z))
        self.dW = np.dot(self.x.T, self.delta)
        self.db = np.dot(np.ones([1, num_examples]), self.delta)
        
    def updateweight(self, learning_rate):
        self.dW += self.reg_lambda * self.W
        self.W += -learning_rate * self.dW
        self.b += -learning_rate * self.db
        
        

class Data(object):
    """
    This class defines data format
    """
    def __init__(self, X):
        self.output = X
        self.output_dim = X.shape[1]
    
        
def main():
    # generate and visualize Make-Moons dataset
    X, y = generate_data()
    plt.scatter(X[:, 0], X[:, 1], s=40, c=y, cmap=plt.cm.Spectral)
    plt.show()

    model = DeepNeuralNetwork([5,5,5,5,2], X, y)
    model.network_setup()
    model.fit_model(X,y)
    model.visualize_decision_boundary(X,y)

if __name__ == "__main__":
    main()